"""
Docstring.
"""
import numpy as np
import time

RIEGEL_EXP = 1.06
MILES_TO_KM = 1.609344
KM_TO_MILES = 1 / MILES_TO_KM


def convert_to_secs(time_string, string_format, **kwargs):
    """"
    Returns time duration in seconds (or np.nan) for input string and associated format.
    e.g. convert_to_secs('37:10', '%M:%S') = 2230
    convert_to_secs('12:34:33', '%H:%M:%S') = 45273
    convert_to_secs('00:03:27.930000', '%H:%M:%S') = 207.93

    Optional fill_zero = True argument if you want to fill problematic answers with 0 instead of np.nan
    """

    fraction = 0

    if '.' in time_string:
        time_string, fraction = time_string.split('.')
        fraction = float('0.' + fraction)

    try:
        time_object = time.strptime(time_string, string_format)
        time_secs = (fraction +
                     time_object.tm_sec +
                     60 * time_object.tm_min +
                     3600 * time_object.tm_hour)

    except (TypeError, ValueError):

        try:
            fill_value = kwargs['fill_zero']
            if fill_value:
                time_secs = 0
            else:
                time_secs = np.nan
        except KeyError:
            time_secs = np.nan

    return time_secs


def calculate_riegel(measured_race_distance, measured_time_secs, race_distance_to_predict):
    """"Returns Riegel time: that is the predicted race time extrapolated from a known race time/distance.
    Distances can be given in any unit, so long as the measured/predicted units are the same."""

    riegel_time = np.power((race_distance_to_predict / measured_race_distance), RIEGEL_EXP) * measured_time_secs

    return riegel_time
