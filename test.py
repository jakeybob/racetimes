import numpy as np
import pandas as pd
import matplotlib.pyplot as mpl
import time
import datetime

RIEGEL_EXP = 1.06
MILES_TO_KM = 1.609344
KM_TO_MILES = 1 / MILES_TO_KM


def convert_to_secs(time_string, string_format, **kwargs):
    """"
    Returns time duration in seconds (or np.nan) for input string and associated format.
    e.g. convert_to_secs('37:10', '%M:%S') = 2230
    convert_to_secs('12:34:33', '%H:%M:%S') = 45273

    Optional fill_zero = True argument if you want to fill problematic answers with 0 instead of np.nan
    """
    try:
        time_object = time.strptime(time_string, string_format)
        time_secs = (time_object.tm_sec +
                     60 * time_object.tm_min +
                     3600 * time_object.tm_hour)

    except (TypeError, ValueError):

        try:
            fill_value = kwargs['fill_zero']
            if fill_value:
                time_secs = 0
            else:
                time_secs = np.nan
        except KeyError:
            time_secs = np.nan

    return time_secs


def calculate_riegel(measured_race_distance, measured_time_secs, race_distance_to_predict):
    """"Returns Riegel time: that is the predicted race time extrapolated from a known race time/distance.
    Distances can be given in any unit, so long as the measured/predicted units are the same."""

    riegel_time = np.power((race_distance_to_predict / measured_race_distance), RIEGEL_EXP) * measured_time_secs

    return riegel_time


if __name__ == "__main__":
    garscube5miles_csv = 'data/Garscube/gu5miles-handicaps-2017-CASEY.csv'

    df = pd.read_csv(garscube5miles_csv, sep=',', header=0, index_col=0)  # create dataframe from csv

    # convert times to seconds
    df['Recent10kTime'] = df['Recent10kTime'].map(lambda x: convert_to_secs(x, '%M:%S', fill_zero=True))
    df['Recent5kTime'] = df['Recent5kTime'].map(lambda x: convert_to_secs(x, '%M:%S', fill_zero=True))
    df['Recent5mileTime'] = df['Recent5mileTime'].map(lambda x: convert_to_secs(x, '%M:%S', fill_zero=True))
    df['UofG5mile'] = df['UofG5mile'].map(lambda x: convert_to_secs(x, '%M:%S'))

    winning_time = convert_to_secs('25:56', '%M:%S')

    df.drop('RaceWonTime', axis=1, inplace=True)  # no need for this column

    # massive bodge, only works as each runner only has one previous race listed
    df['Riegel5mile'] = (df['Recent10kTime'].map(lambda x: calculate_riegel(10, x, 5 * MILES_TO_KM)) +
                         df['Recent5kTime'].map(lambda x: calculate_riegel(5, x, 5 * MILES_TO_KM)) +
                         df['Recent5mileTime'])

    df['Handicap'] = df['Riegel5mile'] - winning_time
    df['UofG5mileHandicapped'] = df['UofG5mile'] - df['Handicap']

    # format this into mins/secs
    df['formatted'] = df['UofG5mileHandicapped'].map(lambda x: str(datetime.timedelta(seconds=x)))

    print(df)
    print(df.sort_values('UofG5mileHandicapped'))
