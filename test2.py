from race_utils import *
import pandas as pd

store = pd.HDFStore('data/Garscube/store.h5')

# dfONE = store['dfONE']
# dfTWO = store['dfTWO']
# dfTHREE = store['dfTHREE']
# dfFOUR = store['dfFOUR']
# dfFIVE = store['dfFIVE']
# dfSIX = store['dfSIX']
# dfEIGHT = store['dfEIGHT']
# dfTEN = store['dfTEN']

# GH_members = store['GH_members']
# all_runners = store['all_runners']
all_races = store['all_races']

store.close()


if __name__ == "__main__":
    # df = all_races.loc['MoragCaseyF']  # all of one runner's data for all races
    df = all_races.loc['MoragCaseyF'].loc['dfONE']  # all of one runner's data for one race
    df = all_races.xs('dfONE', level=1)  # all of one races data (equiv to e.g. dfONE dataframe from store.h5)
    df = all_races.xs('MoragCaseyF', level=0) # all of one runner's data for all races
    df = all_races.xs('dfONE', level=1).loc['MoragCaseyF'] # all of one runner's data for one race

    df = all_races.xs('dfTWO', level=1)  # all female Garscube Harriers who finished race 2 in <1000 seconds
    df = df[(df['Time'] < 1000) & (df['isGH'] == True) & (df['Gender'] == 'F')]

    print(df)


