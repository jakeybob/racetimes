from race_utils import *
import pandas as pd

xlpath_women = 'data/Garscube/GH_winter1617_women.xls'
xlpath_men = 'data/Garscube/GH_winter1617_men.xls'
time_format = '%H:%M:%S'
store = pd.HDFStore('data/Garscube/store.h5')


# create set of Garscube Harriers members (female)
GHf = pd.read_excel(xlpath_women, sheetname='Base', skiprows=[1], parse_cols=[2, 3])
GHf = GHf.dropna(axis=0, how='all')  # drop rows that are all NaN
GHf['Gender'] = 'F'
GHf['Unique ID'] = GHf['Firstname'].map(str) + GHf['Surname'] + GHf['Gender']
GHf.set_index(['Unique ID'], inplace=True, drop=True)

# create set of Garscube Harriers members (male)
GHm = pd.read_excel(xlpath_men, sheetname='Base', skiprows=[1], parse_cols=[2, 3])
GHm = GHm.dropna(axis=0, how='all')  # drop rows that are all NaN
GHm['Gender'] = 'M'
GHm.loc[GHm['Surname'] == 'Maciver', 'Surname'] = 'MacIver'  # fix typo
GHm['Unique ID'] = GHm['Firstname'].map(str) + GHm['Surname'] + GHm['Gender']
GHm.set_index(['Unique ID'], inplace=True, drop=True)

GH_members = GHf.append(GHm)
GH_members['isGH'] = True
GH_members.rename(columns={'Firstname': 'Name'}, inplace=True)

store['GH_members'] = GH_members  # write to the hdf5 store
print("GH members list ... done")


#####################
race_id = 'ONE'

# women's data
dfONEwomen = pd.read_excel(xlpath_women, sheetname=race_id, header=0, skiprows=[1], parse_cols=4)
dfONEwomen = dfONEwomen.dropna(axis=0, how='any')  # drop rows that are have NaNs
dfONEwomen['Unique ID'] = dfONEwomen['Name'].map(str) + dfONEwomen['Surname'] + dfONEwomen['Gender']  # make Unique ID
dfONEwomen.set_index(['Unique ID'], inplace=True, drop=True)

# men's data
dfONEmen = pd.read_excel(xlpath_men, sheetname=race_id, header=0, skiprows=[1], parse_cols=4)
dfONEmen = dfONEmen.dropna(axis=0, how='any')  # drop rows that have NaNs
dfONEmen['Unique ID'] = dfONEmen['Name'].map(str) + dfONEmen['Surname'] + dfONEmen['Gender']  # reconstruct Unique ID
dfONEmen.set_index(['Unique ID'], inplace=True, drop=True)

# all data
dfONE = dfONEwomen.append(dfONEmen)
dfONE['Time'] = dfONE['Time'].map(lambda x: convert_to_secs(str(x), time_format))

dfONE['Handicap'] = np.nan
dfONE['isGH'] = dfONE.index.isin(GH_members.index.values)    # is runner a Garscube Harrier?
dfONE['Distance'] = 4
dfONE['Type'] = 'XC'

store['dfONE'] = dfONE  # write to the hdf5 store
print("Race 1 ... done")


#####################
race_id = 'TWO'

# women and men's data is all together
dfTWO = pd.read_excel(xlpath_women, sheetname=race_id, header=0, skiprows=[1], parse_cols=4)
dfTWO = dfTWO.dropna(axis=0, how='any')  # drop rows that are have NaNs

dfTWO['Surname'][dfTWO['Surname'] == 'Mckellar'] = 'McKellar'  # fix typo

dfTWO['Unique ID'] = dfTWO['Name'].map(str) + dfTWO['Surname'] + dfTWO['Gender']  # make Unique ID
dfTWO.set_index(['Unique ID'], inplace=True, drop=True)

dfTWO['Time'] = dfTWO['Time'].map(lambda x: convert_to_secs(str(x), time_format))

dfTWO['Handicap'] = np.nan
dfTWO['isGH'] = dfTWO.index.isin(GH_members.index.values)    # is runner a Garscube Harrier?
dfTWO['Distance'] = 4
dfTWO['Type'] = 'XC'

store['dfTWO'] = dfTWO  # write to the hdf5 store
print("Race 2 ... done")


#####################
race_id = 'THREE'

# women's data
dfTHREEwomen = pd.read_excel(xlpath_women, sheetname=race_id, header=0, skiprows=[1], parse_cols=4)
dfTHREEwomen = dfTHREEwomen.dropna(axis=0, how='any')  # drop rows that are have NaNs
dfTHREEwomen['Unique ID'] = dfTHREEwomen['Name'].map(str) + dfTHREEwomen['Surname'] + dfTHREEwomen['Gender']
dfTHREEwomen.set_index(['Unique ID'], inplace=True, drop=True)

# men's data
dfTHREEmen = pd.read_excel(xlpath_men, sheetname=race_id, header=0, skiprows=[1], parse_cols=4)
dfTHREEmen = dfTHREEmen.dropna(axis=0, how='any')  # drop rows that are have NaNs
dfTHREEmen['Gender'] = 'M'  # at least one has missing gender in original file
dfTHREEmen['Unique ID'] = dfTHREEmen['Name'].map(str) + dfTHREEmen['Surname'] + dfTHREEmen['Gender']  # make Unique ID
dfTHREEmen.set_index(['Unique ID'], inplace=True, drop=True)

# all data
dfTHREE = dfTHREEwomen.append(dfTHREEmen)
dfTHREE['Time'] = dfTHREE['Time'].map(lambda x: convert_to_secs(str(x), time_format))

dfTHREE['Handicap'] = np.nan
dfTHREE['isGH'] = dfTHREE.index.isin(GH_members.index.values)    # is runner a Garscube Harrier?
dfTHREE['Distance'] = 4
dfTHREE['Type'] = 'XC'

# print(dfTHREE[dfTHREE.index.duplicated()])  # test to see if more than one runner with same name/ID
dfTHREE = dfTHREE[~dfTHREE.index.duplicated(keep=False)]  # no way to distinguish runner with same IDs, so just delete

store['dfTHREE'] = dfTHREE  # write to the hdf5 store
print("Race 3 ... done")


#####################
race_id = 'FOUR'

# women's data
dfFOURwomen = pd.read_excel(xlpath_women, sheetname=race_id, header=0, skiprows=[1], parse_cols=4)
dfFOURwomen = dfFOURwomen.dropna(axis=0, how='any')  # drop rows that are have NaNs
dfFOURwomen['Gender'] = 'F'
dfFOURwomen['Unique ID'] = dfFOURwomen['Name'].map(str) + dfFOURwomen['Surname'] + dfFOURwomen['Gender']
dfFOURwomen.set_index(['Unique ID'], inplace=True, drop=True)

# men's data
dfFOURmen = pd.read_excel(xlpath_men, sheetname=race_id, header=0, skiprows=[1], parse_cols=4)
dfFOURmen = dfFOURmen.dropna(axis=0, how='any')  # drop rows that are have NaNs
dfFOURmen['Gender'] = 'M'
dfFOURmen['Unique ID'] = dfFOURmen['Name'].map(str) + dfFOURmen['Surname'] + dfFOURmen['Gender']  # make Unique ID
dfFOURmen.set_index(['Unique ID'], inplace=True, drop=True)

# all data
dfFOUR = dfFOURwomen.append(dfFOURmen)
dfFOUR['Time'] = dfFOUR['Time'].map(lambda x: convert_to_secs(str(x), time_format))

dfFOUR['Handicap'] = np.nan
dfFOUR['isGH'] = dfFOUR.index.isin(GH_members.index.values)    # is runner a Garscube Harrier?
dfFOUR['Distance'] = 4
dfFOUR['Type'] = 'XC'

store['dfFOUR'] = dfFOUR  # write to the hdf5 store
print("Race 4 ... done")


#####################
race_id = 'FIVE'

# women's data
dfFIVEwomen = pd.read_excel(xlpath_women, sheetname=race_id, header=0, skiprows=[1], parse_cols=4)
dfFIVEwomen = dfFIVEwomen.dropna(axis=0, how='any')  # drop rows that are have NaNs
dfFIVEwomen['Gender'] = 'F'
dfFIVEwomen['Unique ID'] = dfFIVEwomen['Name'].map(str) + dfFIVEwomen['Surname'] + dfFIVEwomen['Gender']
dfFIVEwomen.set_index(['Unique ID'], inplace=True, drop=True)

# men's data
dfFIVEmen = pd.read_excel(xlpath_men, sheetname=race_id, header=0, skiprows=[1], parse_cols=4)
dfFIVEmen = dfFIVEmen.dropna(axis=0, how='any')  # drop rows that are have NaNs
dfFIVEmen['Gender'] = 'M'
dfFIVEmen['Name'][dfFIVEmen['Name'] == 'patrick'] = 'Patrick'  # fix typo
dfFIVEmen['Unique ID'] = dfFIVEmen['Name'].map(str) + dfFIVEmen['Surname'] + dfFIVEmen['Gender']  # make Unique ID
dfFIVEmen.set_index(['Unique ID'], inplace=True, drop=True)

# all data
dfFIVE = dfFIVEwomen.append(dfFIVEmen)
dfFIVE['Time'] = dfFIVE['Time'].map(lambda x: convert_to_secs(str(x), time_format))

dfFIVE['Handicap'] = np.nan
dfFIVE['isGH'] = dfFIVE.index.isin(GH_members.index.values)    # is runner a Garscube Harrier?
dfFIVE['Distance'] = 4*MILES_TO_KM
dfFIVE.loc[dfFIVE['Gender'] == 'M', 'Distance'] = 6*MILES_TO_KM  # blokes ran 6 miles
dfFIVE['Type'] = 'XC'

store['dfFIVE'] = dfFIVE  # write to the hdf5 store
print("Race 5 ... done")


#####################
race_id = 'SIX'

# women and men's data is all together
dfSIX = pd.read_excel(xlpath_women, sheetname=race_id, header=0, skiprows=[1], parse_cols=5)
dfSIX = dfSIX.dropna(axis=0, how='any')  # drop rows that are have NaNs

dfSIX['Unique ID'] = dfSIX['Name'].map(str) + dfSIX['Surname'] + dfSIX['Gender']  # make Unique ID
dfSIX.set_index(['Unique ID'], inplace=True, drop=True)

dfSIX['Time'] = dfSIX['Time'].map(lambda x: convert_to_secs(str(x), time_format))
dfSIX['Handicap'] = dfSIX['Handicap'].map(lambda x: convert_to_secs(str(x), time_format))

dfSIX['isGH'] = dfSIX.index.isin(GH_members.index.values)    # is runner a Garscube Harrier?
dfSIX['Distance'] = 5*MILES_TO_KM
dfSIX['Type'] = 'ROAD'

store['dfSIX'] = dfSIX  # write to the hdf5 store
print("Race 6 ... done")


#####################
race_id = 'EIGHT'       # EIGHT is the same race as SEVEN but with handicap data

# women's data
dfEIGHTwomen = pd.read_excel(xlpath_women, sheetname=race_id, header=0, skiprows=[1], parse_cols=5)
dfEIGHTwomen['Gender'] = 'F'
dfEIGHTwomen = dfEIGHTwomen.dropna(axis=0, how='any')  # drop rows that are have NaNs

dfEIGHTwomen['Handicap'] = dfEIGHTwomen['Handicap'].map(lambda x: convert_to_secs(str(x), time_format))
dfEIGHTwomen['Distance'] = 6.4  # women ran 6.4 km
dfEIGHTwomen['Unique ID'] = dfEIGHTwomen['Name'].map(str) + dfEIGHTwomen['Surname'] + dfEIGHTwomen['Gender']
dfEIGHTwomen.set_index(['Unique ID'], inplace=True, drop=True)


# men's data
dfEIGHTmen = pd.read_excel(xlpath_men, sheetname=race_id, header=0, skiprows=[1], parse_cols=5)
dfEIGHTmen['Gender'] = 'M'
dfEIGHTmen = dfEIGHTmen.dropna(axis=0, how='any')  # drop rows that are have NaNs

dfEIGHTmen['Handicap'] = dfEIGHTmen['Handicap'].map(lambda x: convert_to_secs(str(x), time_format))
dfEIGHTmen['Distance'] = 10.0  # men ran 10 km
dfEIGHTmen['Unique ID'] = dfEIGHTmen['Name'].map(str) + dfEIGHTmen['Surname'] + dfEIGHTmen['Gender']  # make Unique ID
dfEIGHTmen.set_index(['Unique ID'], inplace=True, drop=True)


# all data
dfEIGHT = dfEIGHTwomen.append(dfEIGHTmen)
dfEIGHT['Time'] = dfEIGHT['Time'].map(lambda x: convert_to_secs(str(x), time_format))
dfEIGHT['isGH'] = dfEIGHT.index.isin(GH_members.index.values)    # is runner a Garscube Harrier?
dfEIGHT['Type'] = 'XC'
dfEIGHT.loc[dfEIGHT.index == 'RaghbirSinghM', 'Distance'] = 7.5  # only did three out of four laps

store['dfEIGHT'] = dfEIGHT  # write to the hdf5 store
print("Races 7/8 ... done")


#####################
race_id = 'TEN'       # TEN is the same race as NINE but with handicap data

# women's data
dfTENwomen = pd.read_excel(xlpath_women, sheetname=race_id, header=0, skiprows=[1], parse_cols=5)
dfTENwomen['Gender'] = 'F'
dfTENwomen = dfTENwomen.dropna(axis=0, how='any')  # drop rows that are have NaNs

dfTENwomen.loc[dfTENwomen['Surname'] == 'MacDougall', 'Surname'] = 'Macdougall'  # fix typo
dfTENwomen['Unique ID'] = dfTENwomen['Name'].map(str) + dfTENwomen['Surname'] + dfTENwomen['Gender']
dfTENwomen.set_index(['Unique ID'], inplace=True, drop=True)

# men's data
dfTENmen = pd.read_excel(xlpath_men, sheetname=race_id, header=0, skiprows=[1], parse_cols=5)
dfTENmen['Gender'] = 'M'
dfTENmen = dfTENmen.dropna(axis=0, how='any')  # drop rows that are have NaNs

dfTENmen.loc[dfTENmen['Surname'] == 'Maciver', 'Surname'] = 'MacIver'  # fix typo
dfTENmen['Unique ID'] = dfTENmen['Name'].map(str) + dfTENmen['Surname'] + dfTENmen['Gender']  # make Unique ID
dfTENmen.set_index(['Unique ID'], inplace=True, drop=True)

# all data
dfTEN = dfTENwomen.append(dfTENmen)
dfTEN['Time'] = dfTEN['Time'].map(lambda x: convert_to_secs(str(x), time_format))
dfTEN['Handicap'] = dfTEN['Handicap'].map(lambda x: convert_to_secs(str(x), time_format))
dfTEN['isGH'] = dfTEN.index.isin(GH_members.index.values)    # is runner a Garscube Harrier?
dfTEN['Distance'] = 10
dfTEN['Type'] = 'XC'

# need to deal with two young runners who ran different distances, original times appear in sheet NINE in .xls
dfTEN.loc[dfTEN.index == 'HannahBerryF', 'Distance'] = 6.8  # only did 6.8 km
dfTEN.loc[dfTEN.index == 'HannahBerryF', 'Time'] = convert_to_secs('00:32:55', time_format)
dfTEN.loc[dfTEN.index == 'IsabellaSheldonF', 'Distance'] = 6.8
dfTEN.loc[dfTEN.index == 'IsabellaSheldonF', 'Time'] = convert_to_secs('00:31:14', time_format)

store['dfTEN'] = dfTEN  # write to the hdf5 store
print("Races 9/10 ... done")


#####################
# create a list of all the unique runners (dropping dupes as impossible to distinguish without more info)
df_list = [dfONE, dfTWO, dfTHREE, dfFOUR, dfFIVE, dfSIX, dfEIGHT, dfTEN]
df_tuple = ((dfONE, 'dfONE'), (dfTWO, 'dfTWO'), (dfTHREE, 'dfTHREE'), (dfFOUR, 'dfFOUR'), (dfFIVE, 'dfFIVE'),
            (dfSIX, 'dfSIX'), (dfEIGHT, 'dfEIGHT'), (dfTEN, 'dfTEN'))


all_runners = pd.DataFrame()

for df in df_list:
    df_cropped = df.drop(['Distance', 'Handicap', 'Time', 'Type'], axis=1)   # drop unwanted columns
    all_runners = all_runners.append(df_cropped)

all_runners = all_runners.append(GH_members)
all_runners = all_runners[~all_runners.index.duplicated(keep='last')]  # drop dupes
print(all_runners.shape)

store['all_runners'] = all_runners  # write to the hdf5 store
print("List of all runners ... done")


# create a dataframe with all the race data; every runner who ran at least once, with their race data for each race,
# stored in multiindex format, with runner's Unique ID as primary index, race ID as secondary index
all_races = pd.DataFrame()

for runner in all_runners.index:
    for race_number, df in enumerate(df_list):
        if runner in df.index:
            all_races = all_races.append(pd.DataFrame([df.loc[runner]],
                                                      index=[[runner], [df_tuple[race_number][1]]],
                                                      columns=df.columns))


store['all_races'] = all_races
print("All race data ... done")

store.close()
